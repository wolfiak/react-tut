import React, { Component } from 'react';
import './App.scss';
import './scss/main.scss';
import Projects from './components/Projects';
import Todos from './components/Todos';
import AddProject from './components/AddProject';
import  uuid from 'uuid';

//import Home from './views/Home';


class App extends Component {
  constructor(){
    super();
    this.state= {
      projects: [],
      todos: []
    }
  }
  getTodods(){
    fetch('https://jsonplaceholder.typicode.com/todos').then((res)=>{
      console.log(res);
      console.log();
      res.json().then((reso)=>{
        console.log(reso);
        this.setState({
          todos: reso
        });
      });
    });
  }
  getProjects(){
    this.setState({projects: [
      {
        id: uuid.v4(),
        title: 'Biznes web',
        category: 'Web designe'
      },
      {
        id: uuid.v4(),
        title: 'Social app',
        category: 'mobile deb'
      },
      {
        id: uuid.v4(),
        title: 'E commencer',
        category: 'Web dev'
      }
    ]});
  }
  componentWillMount(){
    this.getProjects();
    this.getTodods();
  }

  componentDidMount(){
    this.getTodods();
  }
  handleAddProject(project){
    console.log(`Works from app.js: ${project.title}`);
    let projects=this.state.projects;
    projects.push(project);
    this.setState({
      projects: projects
    });
  }
  hadnleDelete(id){
    console.log(`Id w App.js ${id}`);
    let projects= this.state.projects;
    let index=projects.findIndex(x=> x.id === id);
    projects.splice(index,1);
    this.setState({
      projects: projects
    });

  }
  render() {
    return (
      <div className="App">
        <AddProject addProject={this.handleAddProject.bind(this)}></AddProject>
        <Projects onDelete={this.hadnleDelete.bind(this)} projects= {this.state.projects}></Projects>
        <Todos todos={this.state.todos}></Todos>
      </div>
    );
  }
}

export default App;
