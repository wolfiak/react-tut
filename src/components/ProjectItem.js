import React, { Component } from 'react';


class ProjectItem extends Component {

  deleteProject(id){
    this.props.onDelete(id);
  }

  render() {
      console.log(this.props);
      let proj=this.props.project;
    return (
      <li className="Project">
        <strong>{proj.title} </strong> - {proj.category} <a href="#" onClick={this.deleteProject.bind(this,proj.id)}>Usun</a>
      </li>
    );
  }
}

export default ProjectItem;
