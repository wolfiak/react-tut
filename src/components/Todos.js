import React, { Component } from 'react';
import TodoItem from './TodoItem';

class Todos extends Component {
 

  render() {
    let todsItem;
    if(this.props.todos){
        todsItem=this.props.todos.map(todos =>{
            return(
                <ul key={todos.title}>
                    <TodoItem item={todos}></TodoItem>
                </ul>
                
            );
        });
    }
     
    return (
      <div className="tods">
        <h3>Todos</h3>
        {todsItem}
      </div>
    );
  }
}



export default Todos;
